import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:glitz_sample_project/colors/my_colors.dart';
import 'package:glitz_sample_project/common_widgets/common_alerts.dart';
import 'package:glitz_sample_project/common_widgets/common_divider.dart';
import 'package:glitz_sample_project/common_widgets/common_text_field.dart';
import 'package:glitz_sample_project/common_widgets/common_text_view.dart';

import '../bloc/login_bloc.dart';
import 'home_screen.dart';

///...in here i have used bloc for state management oi other screen its used provider

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.backgroundBlue,
        title: const Text('Login'),
      ),
      body: LoginForm(),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  String usernameError = "";
  String passwordError = "";
  bool isObscured = true;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(16.0),
        child: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is UsernameError) {
              passwordError = "";
              usernameError = state.error!["username"] ?? "";
            } else if (state is PasswordError) {
              passwordError = state.error!["password"] ?? "";
              usernameError = "";
            } else if (state is LoginFailed) {
             CommonAlerts().showSnackBar(context: context,message: "Login Failed");

            } else if (state is LoginSuccess) {
              passwordError = "";
              usernameError = "";
              _passwordController.clear();
              _emailController.clear();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeScreen(),
                  ));
            }
          },
          builder: (BuildContext context, LoginState state) {
            return screenBodyPart();
          },
        ));
  }

  screenBodyPart() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CommonTextField(
          labelText: "UserName",
          controller: _emailController,
          errorText: usernameError,
        ),
        CommonTextField(
          labelText: "Password",
          controller: _passwordController,
          errorText: passwordError,
        ),
        CommonDivider(height: 16.0),
        ElevatedButton(
          onPressed: () {

            context.read<LoginBloc>().add(LoginClicked(
                username: _emailController.text,
                password: _passwordController.text));
          },
          child: CommonTextView(
            text: 'Login',
          ),
        ),
      ],
    );
  }
}
