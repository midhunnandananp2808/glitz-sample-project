import 'package:flutter/material.dart';
import 'package:glitz_sample_project/colors/my_colors.dart';
import 'package:glitz_sample_project/common_widgets/common_text_view.dart';
import 'package:glitz_sample_project/screens/video_player_screen.dart';
import 'package:provider/provider.dart';

import '../providers/home_screen_provider.dart';

class FavouriteVideosScreen extends StatefulWidget {
  @override
  State<FavouriteVideosScreen> createState() => _FavouriteVideosScreenState();
}

class _FavouriteVideosScreenState extends State<FavouriteVideosScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: CommonTextView(
          text: 'Favourite Video List',
          maxLines: 2,
          color: MyColors.white,
        ),
        backgroundColor: MyColors.backgroundBlue,
        foregroundColor: MyColors.white, // Customize app bar color
      ),
      body: VideoList(),
    );
  }
}

class VideoList extends StatefulWidget {
  @override
  _VideoListState createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> {
  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenProvider>(
      builder: (context, value, child) {
        return value.favouriteVideosList.isNotEmpty?ListView.builder(
          itemCount: value.favouriteVideosList.length,
          itemBuilder: (context, index) {
            final video = value.favouriteVideosList[index];
            return Padding(
              padding:
              const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
              child: Column(
                children: [
                  Container(
                    // Set the width to 50
                    height: 50, // Set the height to 50
                    color: Colors.blue, // Set the background color
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CommonTextView(
                          text: video['title']!,
                          color: Colors.white, // Set the text color
                          fontWeight: FontWeight.bold,
                        ),
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.favorite,
                              color: MyColors.cherryRed,
                            )),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        VideoPlayerHome(
                                            videoLink: video['thumbnail']!),
                                  ));
                            },
                            icon: const Icon(
                              Icons.chevron_right_rounded,
                              color: MyColors.white,
                            )),
                      ],
                    ),
                  ),
                  Container(
                    height: 50, // Set the height to 50
                    color: MyColors.yellow, // Set the background color
                    child: CommonTextView(
                      text: video['thumbnail']!,
                      color: Colors.white, // Set the text color
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
          },
        ):Center(child: CommonTextView(text: "You don't have any Favourites videos",fontWeight: FontWeight.bold,fontSize: 16,),);
      },
    );
  }

  void _playVideo(String videoTitle) {
    print('Playing video: $videoTitle');
  }
}
