import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:glitz_sample_project/colors/my_colors.dart';
import 'package:glitz_sample_project/common_widgets/common_text_view.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import '../providers/video_screen_provider.dart';

class VideoPlayerHome extends StatefulWidget {
  String videoLink;
   VideoPlayerHome({super.key, required this.videoLink});
  @override
  State<VideoPlayerHome> createState() => _VideoPlayerHomeState();
}

class _VideoPlayerHomeState extends State<VideoPlayerHome> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.backgroundBlue,
        foregroundColor: MyColors.white,
        title: CommonTextView(text: 'Video Player',color: MyColors.white,),
      ),
      body:OrientationBuilder(builder: (context, orientation) {
        if (orientation == Orientation.portrait) {
          return VideoPlayerScreen(
              isScreenLadScape: false,videoUrl: widget.videoLink,); // Build portrait layout
        } else {
          return VideoPlayerScreen(
              isScreenLadScape: true,videoUrl: widget.videoLink,); // Build landscape layout
        }
      }),
    );
  }
}

class VideoPlayerScreen extends StatefulWidget {
   String videoUrl;
  bool isScreenLadScape = false;

  VideoPlayerScreen({required this.videoUrl,required this.isScreenLadScape});

  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  VideoScreenProvider? videoScreenProvider;

  @override
  void initState() {
    super.initState();
    videoScreenProvider=Provider.of<VideoScreenProvider>(context,listen: false);
    videoScreenProvider!.initialiseVideo(widget.videoUrl);
  }

  @override
  void dispose() {
    super.dispose();
    videoScreenProvider!.controller.dispose();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  }

  @override
  Widget build(BuildContext context) {
    return Consumer<VideoScreenProvider>(builder: (context, value, child) {
      return value.controller.value.isInitialized
          ? Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(
            left: widget.isScreenLadScape ? 70 : 0,
            right: widget.isScreenLadScape ? 70 : 0,
            bottom: 5),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Column(
        children: [
                Stack(
                  children: [
                    AspectRatio(
                      aspectRatio: value.controller.value.aspectRatio,
                      child: VideoPlayer(value.controller),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: VideoProgressIndicator(
                        padding:  EdgeInsets.only(
                            left: 45,
                            top:  widget.isScreenLadScape ? 270 : 170,
                            bottom: 20,
                            right: 100),
                        colors: const VideoProgressColors(
                            backgroundColor: Colors.white,
                            playedColor: Colors.green,
                            bufferedColor: Colors.grey),
                        value.controller,
                        allowScrubbing: true,
                      ),
                    ),Positioned(
                      top:  widget.isScreenLadScape ? 253 : 153,
                      child: IconButton(
                        color: Colors.white,
                        icon: Icon(
                          value.controller.value.isPlaying || !value.isVideoCompleted
                              ? Icons.pause
                              : Icons.play_arrow,
                          size: 40,
                        ),
                        onPressed: () {
                          value.pauseOrPlay();
                        },
                      ),
                    ),Align(
                      alignment: Alignment.bottomRight,
                      child: Padding(
                        padding:  EdgeInsets.only(
                            top: widget.isScreenLadScape ? 265 : 165, right: 20),
                        child: ValueListenableBuilder(
                          valueListenable: value.controller,
                          builder: (context, VideoPlayerValue value, child) {
                            //Do Something with the value.
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "${value.position.inSeconds} /",
                                  style: const TextStyle(fontSize: 8, color: Colors.white),
                                ),
                                Text(
                                  "${value.duration} ",
                                  style: const TextStyle(fontSize: 8, color: Colors.white),
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ),Positioned(
                      top:  widget.isScreenLadScape ? 265 : 165,
                      left: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              IconButton(
                                color: Colors.white,
                                icon: const Icon(
                                  Icons.fast_rewind,
                                  size: 15,
                                ),
                                onPressed: () {
                                  value.rewindVideo();
                                },
                              ),
                              IconButton(
                                color: Colors.white,
                                icon: const Icon(
                                  Icons.fast_forward,
                                  size: 15,
                                ),
                                onPressed: () {
                                  value.fastForwardVideo();
                                },
                              ),
                              IconButton(
                                color: Colors.white,
                                icon: Icon(
                                  value.isVideoMuted
                                      ? Icons.volume_off_sharp
                                      : Icons.volume_down_sharp,
                                  size: 15,
                                ),
                                onPressed: () {
                                  value.handleVolume();
                                },
                              )
                            ],
                          ),
                          Padding(
                            padding:
                            EdgeInsets.only(left:  widget.isScreenLadScape ? 265 : 50),
                            child: Row(
                              children: [
                                IconButton(
                                  color: Colors.white,
                                  icon: const Icon(
                                    Icons.settings,
                                    size: 15,
                                  ),
                                  onPressed: () {},
                                ),
                                IconButton(
                                  color: Colors.white,
                                  icon: const Icon(
                                    Icons.fullscreen,
                                    size: 15,
                                  ),
                                  onPressed: () {
                                    value.toggleOrientation();
                                  },
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    )


                  ],
                ),
        ],
      ),
              ),
            ),
          )
          : const Center(
            child: SizedBox(
            height: 170,
            width: 200,
            child:
            Center(child: CircularProgressIndicator())),
          );
    },);

  }
}
