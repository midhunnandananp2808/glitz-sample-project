import 'package:flutter/material.dart';
import 'package:glitz_sample_project/colors/my_colors.dart';
import 'package:glitz_sample_project/common_widgets/common_alerts.dart';
import 'package:glitz_sample_project/common_widgets/common_text_view.dart';
import 'package:glitz_sample_project/screens/video_player_screen.dart';
import 'package:provider/provider.dart';

import '../providers/home_screen_provider.dart';
import 'favourites_video_screen.dart';
import 'login_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                CommonAlerts().showAlertDialog(
                    context: context,
                    message:
                    "Do you want to Logout",
                    cancelClick: () {
                      Navigator.pop(context);
                    },
                    okClick: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) => const LoginScreen(),));
                    });
              },
              icon: const Icon(
                Icons.logout,
                color: MyColors.red,
              )),
          Row(
            children: [
              CommonTextView(text: "Go to favourites"),
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FavouriteVideosScreen(),
                        ));
                  },
                  icon: const Icon(
                    Icons.favorite,
                    color: MyColors.red,
                  )),
            ],
          ),
        ],
        title: CommonTextView(
          text: 'Video List',
          maxLines: 2,
          color: MyColors.white,
        ),
        backgroundColor: MyColors.backgroundBlue,
        foregroundColor: MyColors.white, // Customize app bar color
      ),
      body: VideoList(),
    );
  }
}

class VideoList extends StatefulWidget {
  @override
  _VideoListState createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> {
  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenProvider>(
      builder: (context, value, child) {
        return ListView.builder(
          itemCount: value.videoData.length,
          itemBuilder: (context, index) {
            final video = value.videoData[index];
            return Padding(
              padding:
              const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
              child: Column(
                children: [
                  Container(
                    // Set the width to 50
                    height: 50, // Set the height to 50
                    color: Colors.blue, // Set the background color
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CommonTextView(
                          text: video['title']!,
                          color: Colors.white, // Set the text color
                          fontWeight: FontWeight.bold,
                        ),
                        IconButton(
                            onPressed: () {
                              CommonAlerts().showAlertDialog(
                                  context: context,
                                  message:
                                  "Do you want to save this in your favourites?",
                                  cancelClick: () {
                                    Navigator.pop(context);
                                  },
                                  okClick: () {
                                    value.addFavouriteVideos(
                                        video: video, context: context);
                                    Navigator.pop(context);
                                  });
                            },
                            icon: const Icon(
                              Icons.favorite,
                              color: MyColors.white,
                            )),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        VideoPlayerHome(
                                            videoLink: video['thumbnail']!),
                                  ));
                            },
                            icon: const Icon(
                              Icons.chevron_right_rounded,
                              color: MyColors.white,
                            )),
                      ],
                    ),
                  ),
                  Container(
                    height: 50, // Set the height to 50
                    color: MyColors.yellow, // Set the background color
                    child: CommonTextView(
                      text: video['thumbnail']!,
                      color: Colors.white, // Set the text color
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  void _playVideo(String videoTitle) {
    print('Playing video: $videoTitle');
  }
}
