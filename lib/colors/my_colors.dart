import 'package:flutter/material.dart';

class MyColors {
  static const Color theme = Color(0xFF443BFF);
  static const Color themeShade = Color(0xD0443BFF);
  static const Color green = Color(0xFFD2F6E6);
  static const Color primaryGreen = Color(0xFF77D1A5);
  static const Color darkGreen = Color(0xFF066B0A);
  static const Color buttonGreen = Color(0xFF0FC275);
  static const Color transparent = Color(0x00000000);
  static const Color appColorLeft = Color(0xFF009A46);
  static const Color appColorRight = Color(0xFF00704A);

  static const Color snackBarBackground = Color(0xFFDBF9F0);

  static const Color editButtonColor = Color(0xFFDAD8FF);
  static const Color deleteButtonColor = Color(0xFFFFF2F2);

  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color orange = Color(0xFFFFA726);
  static const Color red = Color(0xFFE53935);
  static const Color blue = Color(0xFF1151F5);
  static const Color yellow = Color(0xFFEB9900);
  static const Color darkGrey = Color(0xFF171616);
  static const Color lightGrey = Color(0xFFBDBDBD);
  static const Color lineGrey = Color(0xFFE8E5E5);
  static const Color reasonGrey = Color(0xFFEBF3FF);
  static const Color violet = Color(0xFF714cfe);
  static const Color lightBlue = Color(0xffEBF3FF);
  static const Color backgroundGrey = Color(0xFFEEEEEE);
  static const Color lightYellow = Color(0xFFFFC62F);
  static const Color lightRed = Color(0xFFFC5859);
  static const Color grey = Color(0xF078787C);
  static const Color lightWhite = Color(0xFFF4F5F7);
  static const Color lightBlueAccent = Colors.lightBlueAccent;
  static const Color greenAccent = Colors.greenAccent;
  static const Color skyBlue = Color(0xFF3FB4D5);
  static const Color cherryRed = Color(0xFFE1002F);
  static const Color blackishGrey = Color(0xc71a1a1c);
  static const Color backgroundBlue = Color(0xFFDAD8FF);
  static const Color backgroundRed = Color(0xFFF9DEDC);
  static const Color backgroundGreen = Color(0xFFD4FCE1);
  static const Color backgroundYellow = Color(0xFFFFF1D7);
  static const Color lightShadeRed = Color(0xFFF9DEDC);
  static const Color joinButton = Color(0xFF391F4E);
  static const Color userTopColor = Color(0xFFE8F1FF);
  static const Color userBottomColor = Color(0xFFFBFBFB);
}

createGradientColor(Color topColor, Color bottomColor) {
  return LinearGradient(
    colors: [
      topColor,
      bottomColor,
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
}
