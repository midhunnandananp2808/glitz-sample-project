import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';

part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial()) {
    on<LoginEvent>((event, emit) {
      // TODO: implement event handler
    });

    on<LoginClicked>((event, emit) async {
      if (event.username!.trim().isEmpty) {
        emit(UsernameError(error: const {"username": 'Username required'}));
      } else if (event.password!.trim().isEmpty) {
        emit(PasswordError(error: const {"password": 'Password required'}));
      } else if (event.username!.trim() != "glitzadmin@gmail.com" &&
          event.password!.trim() != "Gs0r0e7!E@") {
        emit(LoginFailed());
      }else{
        emit(LoginSuccess());
      }
    });
  }
}
