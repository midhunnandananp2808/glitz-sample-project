part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}
class LoginClicked extends LoginEvent {
  String? username;
  String? password;

  LoginClicked({
    this.username,
    this.password,
  });

}

class LogoutClicked extends LoginEvent {

}