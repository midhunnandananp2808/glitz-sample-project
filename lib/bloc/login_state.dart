part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {
  LoginInitial();
}
class LoginSuccess extends LoginState {

  LoginSuccess();
}

class LoginFailed extends LoginState {

  LoginFailed();
}

class LoginLoading extends LoginState {
  LoginLoading();
}

class LogoutLoading extends LoginState {
  LogoutLoading();
}

class UsernameError extends LoginState {
  Map? error;

  UsernameError({this.error});
}

class PasswordError extends LoginState {
  Map? error;

  PasswordError({this.error});
}