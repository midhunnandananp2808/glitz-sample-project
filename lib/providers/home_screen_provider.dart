import 'package:flutter/material.dart';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_file_downloader/flutter_file_downloader.dart';
import 'dart:io' as io;
import 'package:flutter/cupertino.dart';
import 'package:video_player/video_player.dart';

import '../common_widgets/common_alerts.dart';


class HomeScreenProvider extends ChangeNotifier {

  List<Map<String, String>> favouriteVideosList = [];
  List<Map<String, String>> videoData = [
    {
      'title': 'Video 1',
      'thumbnail':
          "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4",
    },
    {
      'title': 'Video 2',
      'thumbnail':
          "https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4",
    },
    {
      'title': 'Video 3',
      'thumbnail':
          "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4",
    },
    // Add more video data as needed
  ];

addFavouriteVideos({required Map<String, String> video,required BuildContext context}){

  if(favouriteVideosList.contains(video)){
    CommonAlerts().showSnackBar(context: context,message: "This video is already added to favourites");
  }else{
    favouriteVideosList.add(video);
  }
  notifyListeners();
}



}
