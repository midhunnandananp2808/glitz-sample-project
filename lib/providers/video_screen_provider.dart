import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

class VideoScreenProvider extends ChangeNotifier{
  late VideoPlayerController controller;


  int videoIndex = 0;
  Duration? videoDuration;
  bool isVideoPlay = false;
  bool isVideoCompleted = true;
  bool isVideoMuted = false;
  double? progresses;
  String filePath = '';
  ThemeMode currentThemeMode = ThemeMode.light;
  bool isFullScreen = false; // Track full-screen mode

initialiseVideo(String videoUrl){
  controller = VideoPlayerController.networkUrl(Uri.parse(
      videoUrl))
    ..initialize().then((_) {
      // Ensure the first frame is shown
      notifyListeners();
    });
}

  pauseOrPlay() {
    if (controller.value.isPlaying) {
      controller.pause();
    } else {
      if (isVideoCompleted) {
        // If video is completed, seek to the beginning and play
        controller.seekTo(Duration.zero);
      }
      controller.play();
    }
    notifyListeners();
  }

  // Function to toggle screen orientation
  void toggleOrientation() {
    isFullScreen = !isFullScreen;
    if (isFullScreen) {
      SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeRight]);
    } else {
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    }
    notifyListeners();
  }
//  for mute video
  handleVolume() {
    isVideoMuted = !isVideoMuted;
    controller.setVolume(isVideoMuted ? 0.0 : 1.0);
    notifyListeners();
  }

  void rewindVideo() {
    final newPosition =
        controller.value.position - const Duration(seconds: 1);
    controller.seekTo(newPosition);
    notifyListeners();
  }

  void fastForwardVideo() {
    final newPosition =
        controller.value.position + const Duration(seconds: 1);
    if (newPosition < controller.value.duration) {
      controller.seekTo(newPosition);
    } else {
      // Handle reaching the end of the video
    }
    notifyListeners();
  }
}