import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


final lightTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: Colors.blue,
  // Add other light mode theme properties
);

final darkTheme = ThemeData(
  brightness: Brightness.dark,
  primaryColor: Colors.teal,
  // Add other dark mode theme properties
);

class ThemeNotifier with ChangeNotifier {

  ThemeData _currentTheme = lightTheme;

  ThemeData get currentTheme => _currentTheme;

  void changeThemeColor() {
    _currentTheme = (_currentTheme == lightTheme) ? darkTheme : lightTheme;
    notifyListeners();
  }
}