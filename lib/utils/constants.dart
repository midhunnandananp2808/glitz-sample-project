// language options
import 'dart:ui';



//..........Fonts..........//
const String fontMedium = 'Lexend_Medium';
const String fontExtraLight = 'Lexend_ExtraLight';
const String fontLight = 'Lexend_Light';
const String fontRegular = 'Lexend_Regular';
const String fontBold = 'Lexend_Bold';
const String fontSemiBold = 'Lexend_SemiBold';
const String fontExtraBold = 'Lexend_ExtraBold';
const String fontThinLight = 'Lexend_thin';

// Text sizes
const double smallSizeText = 12.0;
const double normalSizeText = 15.0;
const double largeSizeText = 20.0;
const double extraLargeSizeText = 21.0;

// Padding
const double screenPadding = 20.0;

// Margin
const double screenMargin = 20.0;

// Curve
const double curve = 30.0;
const double smallSizeCurve = 20;

// Button
const double buttonHeight = 50.0;

// TextField
const double textFieldHeight = 72.0;
const double textAreaHeight = 62.0;
const double dropDownHeight = 68.0;

// Loader size
const double loaderSize = 180.0;

