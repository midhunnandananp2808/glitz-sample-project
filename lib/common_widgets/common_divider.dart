import 'package:flutter/cupertino.dart';

class CommonDivider extends StatelessWidget {
  double width;
  double height;

  CommonDivider({super.key, this.width = 0.0, this.height = 0.0});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
    );
  }
}
