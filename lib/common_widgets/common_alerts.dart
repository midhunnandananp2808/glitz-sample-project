import 'dart:developer';

import 'package:flutter/material.dart';


import '../colors/my_colors.dart';
import '../utils/constants.dart';
import 'common_divider.dart';
import 'common_text_view.dart';

class CommonAlerts {
  //..snack bar...//
  showSnackBar(
      {required BuildContext context,
      String? message,
      Color? backGroundColor = MyColors.cherryRed}) {
    try {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: const Duration(milliseconds: 1000),
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
        backgroundColor: backGroundColor,
        content: Text(message ?? "",
            style: const TextStyle(
                color: MyColors.white, fontSize: 15.0, fontFamily: fontMedium)),
      ));
    } catch (e) {
      log(e.toString());
    }
  }

//.......common dialog .....//
  showAlertDialog(
      {required BuildContext context,
      String? message,
      String? title,
      String? buttonText,
      Color? okButtonTextColor,
      VoidCallback? okClick,
      VoidCallback? cancelClick,
      bool showCancelButton = true,
      Color? cancelButtonTextColor}) {
    // set up the button
    Widget okButton = Expanded(
      child: ElevatedButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(MyColors.red)),
        onPressed: okClick,
        child: CommonTextView(
          textAlign: TextAlign.center,
          text: buttonText ?? "Ok".toUpperCase(),
          fontSize: 15.0,
          maxLines: 1,
          color: okButtonTextColor ?? MyColors.white,
        ),
      ),
    );
    Widget cancelButton = Expanded(
      child: ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color>(MyColors.buttonGreen)),
        onPressed: cancelClick,
        child: CommonTextView(
          textAlign: TextAlign.center,
          text:"Cancel".toUpperCase(),
          fontSize: 15.0,
          maxLines: 1,
          color: cancelButtonTextColor ?? MyColors.white,
        ),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: CommonTextView(
        textAlign: TextAlign.center,
        text: title ?? 'Alert',
        fontSize: 15.0,
        maxLines: 3,
        color: MyColors.red,
      ),
      actionsPadding: const EdgeInsets.all(15).copyWith(bottom: 10),
      content: CommonTextView(
        textAlign: TextAlign.center,
        text: message,
        fontSize: 15.0,
        maxLines: 10,
        color: MyColors.darkGrey,
      ),
      actions: [
        Row(
          children: [
            if (showCancelButton) ...[cancelButton, CommonDivider(width: 15)],
            okButton,
          ],
        )
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }



}
