import 'package:flutter/material.dart';

import '../colors/my_colors.dart';
import '../utils/constants.dart';

class CommonTextView extends StatelessWidget {
  String? text;
  Color color;
  String fontStyle;
  double fontSize;
  TextDecoration? textDecoration;
  int maxLines;
  TextAlign textAlign;
  VoidCallback? onTap;
  FontWeight fontWeight;

  CommonTextView(
      {Key? key,
      required this.text,
      this.fontWeight = FontWeight.normal,
      this.color = MyColors.black,
      this.fontStyle = fontRegular,
      this.fontSize = normalSizeText,
      this.maxLines = 1,
      this.textAlign = TextAlign.center,
      this.textDecoration,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Text(
        text ?? '',
        maxLines: maxLines,
        overflow: TextOverflow.ellipsis,
        softWrap: true,
        style: TextStyle(
          fontWeight: fontWeight,
          color: color,
          fontFamily: fontStyle,
          fontSize: fontSize,
          decoration: textDecoration,
        ),
        textAlign: textAlign,
      ),
    );
  }
}
