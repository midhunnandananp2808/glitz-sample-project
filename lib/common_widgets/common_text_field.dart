import 'package:flutter/material.dart';

import '../colors/my_colors.dart';
import '../utils/constants.dart';

class CommonTextField extends StatelessWidget {
  String errorText;
  TextEditingController? controller;
  bool obscureText;
  bool? isNumber;
  bool? isEmail;
  int? maxLength;
  TextCapitalization? textCapitalization;
  int maxLine;
  int minLine;
  String labelText;
  String fontFamily;
  Color borderColor;
  Color labelColor;
  double marginTop;
  double contentPadding;
  double height;
  bool isTextArea;
  double marginBottom;
  TextInputAction action;
  TextAlign textAlign;
  Widget? prefixWidget;
  Widget? suffixWidget;
  bool showPrefixWidget;
  bool enableEdit;
  bool showSuffixWidget;
  VoidCallback? onTap;
  bool? readOnly;
  VoidCallback? onEditingComplete;

  CommonTextField(
      {Key? key,
      required this.labelText,
      required this.controller,
      this.fontFamily = fontRegular,
      this.errorText = "",
      this.obscureText = false,
      this.labelColor = MyColors.darkGrey,
      this.maxLength,
      this.borderColor = MyColors.buttonGreen,
      this.isNumber = false,
      this.textCapitalization,
      this.maxLine = 1,
      this.minLine = 1,
      this.isEmail = false,
      this.contentPadding = 10.0,
      this.marginTop = 0.0,
      this.marginBottom = 0.0,
      this.height = textFieldHeight,
      this.isTextArea = false,
      this.action = TextInputAction.next,
      this.textAlign = TextAlign.start,
      this.prefixWidget,
      this.suffixWidget,
      this.showPrefixWidget = false,
      this.enableEdit = true,
      this.showSuffixWidget = false,
      this.onTap,
      this.readOnly,
      this.onEditingComplete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: marginTop, bottom: marginBottom),
      child: SizedBox(
        height: isTextArea ? textAreaHeight : height,
        child: Column(
          children: [
            Expanded(
              child: TextFormField(
                onEditingComplete: onEditingComplete,
                readOnly: readOnly ?? false,
                onTap: onTap,
                enabled: enableEdit,
                textInputAction: action,
                maxLines: maxLine,
                minLines: minLine,
                textCapitalization:
                    textCapitalization ?? TextCapitalization.none,
                textAlign: textAlign,
                controller: controller,
                cursorColor: MyColors.black,
                keyboardType: isNumber!
                    ? const TextInputType.numberWithOptions(
                        signed: true, decimal: true)
                    : isEmail!
                        ? TextInputType.emailAddress
                        : TextInputType.multiline,
                maxLength: maxLength,
                obscureText: obscureText,
                style: TextStyle(
                  color: MyColors.black,
                  fontFamily: fontFamily,
                  fontSize: 15,
                ),
                decoration: showPrefixWidget
                    ? countryPickerDecoration()
                    : showSuffixWidget
                        ? countryPickerDecoration()
                        : defaultDecoration(),
              ),
            ),
            errorText.isNotEmpty
                ? Container(
                    padding: const EdgeInsets.only(
                      bottom: 5.0,
                      left: 5.0,
                    ),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      errorText,
                      maxLines: 1,
                      style: const TextStyle(
                        color: MyColors.red,
                        fontFamily: fontLight,
                        fontSize: 13,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  )
                : const SizedBox()
          ],
        ),
      ),
    );
  }

  boxDecoration(Color color) {
    return BoxDecoration(
      shape: BoxShape.rectangle,
      color: color,
      borderRadius: const BorderRadius.all(
        Radius.circular(10),
      ),
      boxShadow: const [
        BoxShadow(
          color: MyColors.theme,
          blurRadius: 2.0,
        ),
      ],
    );
  }

  defaultDecoration() {
    return InputDecoration(
        counterText: '',
        labelText: labelText,
        labelStyle: TextStyle(
          color: labelColor,
          fontSize: 14,
          fontFamily: fontMedium,
        ),
        contentPadding: EdgeInsets.all(contentPadding),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: const BorderSide(
            color: MyColors.buttonGreen,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(
            color: borderColor,
            width: 1.0,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5.0),
          borderSide: BorderSide(
            color: borderColor,
            width: 1.0,
          ),
        ));
  }

  countryPickerDecoration() {
    return InputDecoration(
      counterText: '',
      contentPadding: showSuffixWidget
          ? const EdgeInsets.only(left: 10.0, right: 5, bottom: 5, top: 5)
          : const EdgeInsets.all(5.0),
      prefixIcon: prefixWidget,
      suffixIcon: suffixWidget,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: borderColor),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: borderColor),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: MyColors.buttonGreen),
      ),
      labelText: labelText,
      labelStyle: TextStyle(
        color: labelColor,
        fontSize: 15,
        fontFamily: fontMedium,
      ),
      filled: true,
      fillColor: Colors.transparent,
    );
  }
}
